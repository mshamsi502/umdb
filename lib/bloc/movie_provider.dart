

import 'package:sqflite/sqflite.dart';
import 'package:umdb/bloc/movies_bloc.dart';
import 'package:umdb/models/movie_model.dart';

import 'package:synchronized/synchronized.dart';

class MovieProvider extends MoviesBloc {
  static final  _lock = Lock();
  String _tableName = 'movies';

  Future<Movie> insert(Movie movie, {conflictAlgorithm: ConflictAlgorithm.ignore}) async {
    List<Movie> sqMovies = await paginate(1);
    bool _same = false;
    return _lock.synchronized(() async {
    print('in Insert to SQLite');
    print('Saved: ${movie.saved}');
    if(sqMovies != null && sqMovies.length > 0) {
      await Future.wait(sqMovies.map((sqMovie) async {
        if (sqMovie.title == movie.title)
          _same = true;
      }
      ));
    }
    if(!_same) {
      await openMovieDB();
      await moviesDB.insert(
          _tableName, movie.toMap(), conflictAlgorithm: conflictAlgorithm);
      await closeMovieDB();
    }
    return movie;
    });
  }

  Future<bool> insertAll(List<Movie> movies) async {
    await Future.wait(movies.map((movie) async {
          if(movie != null)
            await this.insert(movie);
    }
    ));
    return true;

  }
  Future<List<Movie>> paginate(int page, {int limit: 15}) async {
    return _lock.synchronized(() async {
    print('in Paginate to SQLite');
    await openMovieDB();
    List<Map> maps  = await moviesDB.query(_tableName,
        columns: ['Id', 'Title', 'imdbRating', 'Poster', 'Plot', 'Saved'],
        limit: limit,
        offset: page == 1 ? 0 : ((page -1) * limit)
    );
    await closeMovieDB();
    print("length maps from pagination sqlite : ${maps.length}");
    List<Movie> movies = new List<Movie> ();
      if (maps.length > 0) {

        maps.forEach((movie) {
          movies.add(Movie.fromJson(movie));
        }
        );
      }
    print("length movies from pagination sqlite : ${movies.length}");
    return movies;
    });
  }

}