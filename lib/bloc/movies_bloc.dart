import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:umdb/bloc/base_bloc.dart';
import 'package:umdb/bloc/movie_provider.dart';
import 'package:umdb/models/movie_model.dart';

import 'package:synchronized/synchronized.dart';

List<Movie> _movies = new List<Movie> ();
class MoviesBloc implements BaseBloc {
  final  _moviesController = StreamController<List<Movie>>();
  static final  _lock = Lock();
  Database moviesDB;
  String _moviesPath;

  Stream<List<Movie>> get moviesStream => _moviesController.stream;
  List<Movie> get movies => _movies;

  Future<List<Movie>> addMovieToBloc(List<Movie> movies) async {
    // return _lock.synchronized(() async {
    _movies = new List<Movie>();
    _movies.addAll(movies);

    MovieProvider().insertAll(_movies);
    _moviesController.sink.add(movies);
    return _movies;
    // });
  }

  int getLengthMovieList() {
    return _movies.length;
  }
  clearMovieList() {
    _movies.clear();
  }

  Future<void> removeAll() async {
    moviesDB.delete('movies');
    clearMovieList();
    _moviesController.sink.add(_movies);
  }

  Future<void> openMovieDB({String dbName:'movies.db'}) async {

    var databasesPath = await getDatabasesPath();
    _moviesPath = join(databasesPath, dbName);

    moviesDB = await openDatabase(_moviesPath, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(
              'CREATE TABLE movies (Id INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT NOT NULL, imdbRating TEXT, Poster TEXT, Plot TEXT, Saved INTEGER DEFAULT 0)');
        });
  }
  Future<void> closeMovieDB() async => moviesDB.close();

  void dispose() {
    _moviesController.close();
  }


}
