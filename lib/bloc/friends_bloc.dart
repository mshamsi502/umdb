import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:umdb/bloc/base_bloc.dart';
import 'package:umdb/bloc/friend_provider.dart';
import 'package:umdb/models/friend_model.dart';

List<Friend> _friends = new List<Friend> ();
class FriendsBloc implements BaseBloc {
  final  _friendsController = StreamController<List<Friend>>();
  Database friendDB;
  String _friendPath;

  Stream<List<Friend>> get friendsStream => _friendsController.stream;
  List<Friend> get friends => _friends;

  List<Friend> addFriendToBloc(List<Friend> friends) {
    _friends = new List<Friend>();
    _friends.addAll(friends);
    FriendProvider().insertAll(_friends);
    _friendsController.sink.add(friends);
    return _friends;
  }
  int getLengthFriendList() {
    return _friends.length;
  }
  clearFriendList() {
    _friends.clear();
  }


  Future openFriendDB({String dbName:'friends.db'}) async {

    var databasesPath = await getDatabasesPath();
    _friendPath = join(databasesPath, dbName);

    friendDB = await openDatabase(_friendPath, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(
              'CREATE TABLE friends (Id INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT NOT NULL)');
        });
  }
  Future closeFriendDB() async => friendDB.close();

  void dispose() {
    _friendsController.close();
  }

}
