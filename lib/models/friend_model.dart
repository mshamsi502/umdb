// To parse this JSON data, do
//
//     final movie = movieFromJson(jsonString);
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'friend_model.g.dart';
Friend friendFromJson(String str) => Friend.fromJson(json.decode(str));

String friendToJson(Friend data) => json.encode(data.toJson());


@JsonSerializable()
class Friend {
  @JsonKey(name: 'Id')
  int id;
  @JsonKey(name: 'Title')
  String title;

  Friend({
    this.id,
    this.title,

  });



  factory Friend.fromJson(Map<String, dynamic> json) => _$FriendFromJson(json);
  Map<String, dynamic> toJson() => _$FriendToJson(this);

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'Id': id,
      'Title': title,
    };
  }
  @override
  String toString() {
    return 'friend{Id: $id, Title: $title}';
  }

}

