import 'package:umdb/api/movie_api.dart';
import 'package:umdb/models/movie_model.dart';

class MovieRepo {
  final movieApi = MovieAPI();

  Future<Movie> findMovieByName(name) => movieApi.findMovie(name);
}
