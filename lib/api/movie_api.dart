import 'package:http/http.dart' as http;

import 'package:umdb/models/movie_model.dart';

// import '../secret/keys.dart';

class MovieAPI {
  // String apiKey = "13afa821";
  final omdbUrl = 'http://www.omdbapi.com/?apikey=13afa821&t=';

  Future<Movie> findMovie(String name) async {
    try {
      final response = await http.get('$omdbUrl$name');
      if (response.statusCode == 200) {
        print('${movieFromJson}');
        return movieFromJson(response.body);
      } else {
        throw Exception("Error occured while fetching movie");
      }
    } catch (e) {
      print(e);
      print("Error occured during connection");
      return null;
    }
  }
}
