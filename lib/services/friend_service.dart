
import 'package:umdb/models/friend_model.dart';

import 'package:umdb/bloc/Friend_provider.dart';

class FriendService {

  static Future<Map> getAllFriendsFromSqlite(int page) async {
    var db = new FriendProvider();
    await db.openFriendDB();
    List<Friend> friends = await db.paginate(page);
    await db.closeFriendDB();
    return {
      "currentPage": page,
      "friends": friends
    };
  }

  static Future<bool> saveAllFriendsIntoSqlite(List<Friend> friends) async {
    var db = new FriendProvider();
    await db.openFriendDB();
    await db.insertAll(friends);
    await db.closeFriendDB();
    return true;
  }

}