import 'package:flutter/material.dart';
import 'package:umdb/bloc/bloc_provider.dart';
import 'package:umdb/bloc/movie_search_bloc.dart';
import 'package:umdb/models/movie_model.dart';
import 'package:umdb/services/movie_service.dart';
import 'package:umdb/ui_widgets/custom_app_bar.dart';
import 'package:umdb/ui_widgets/movie_detail.dart';

class MovieSearch extends StatefulWidget {
  @override
  _MovieSearchState createState() => _MovieSearchState();
}

class _MovieSearchState extends State<MovieSearch> {
  MovieSearchBloc movieSearchBloc;

  @override
  void initState() {
    super.initState();
    movieSearchBloc = MovieSearchBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MovieSearchBloc>(
      bloc: movieSearchBloc,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize:
              Size.fromHeight(150.0 + MediaQuery.of(context).padding.top),
          child: CustomAppBar(),
        ),
        body: SafeArea(
          child: Container(
            child: StreamBuilder<Movie>(
                stream: movieSearchBloc.movieStream,
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data.title != null && snapshot.data.title != '') {
                    snapshot.data.saved = 0;
                    MovieService().addMovieToHistory(snapshot.data);
                    return MovieDetail(snapshot.data);
                  } else {
                    return Center(
                      child: Text('No Movie yet'),
                    );
                  }
                }),
          ),
        ),
      ),
    );
  }
}
