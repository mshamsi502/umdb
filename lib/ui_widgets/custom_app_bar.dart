import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:umdb/bloc/bloc_provider.dart';
import 'package:umdb/bloc/movie_search_bloc.dart';

class CustomAppBar extends StatelessWidget {
  double appBarHeight(BuildContext context) =>
      150.0 + MediaQuery.of(context).padding.top;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 1.0,
      child: Container(
        height: appBarHeight(context),
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        color: Colors.red,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                children: [
                  GestureDetector(
                    child: new Icon(Icons.arrow_back, color: Colors.white,),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: 15,),
                  Text(
                    'Movie Search',
                    style: DefaultTextStyle.of(context)
                        .style
                        .copyWith(fontSize: 30, color: Colors.white),
                  ),
                ],
              ),
              new Container(
                alignment: Alignment.center,
                // height: 50,
                child: SearchBox(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SearchBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MovieSearchBloc bloc = BlocProvider.of<MovieSearchBloc>(context);
    return Card(
      elevation: 9.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(6.0),
        ),
      ),
      child: TextField(
        textAlignVertical: TextAlignVertical.center ,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Enter a Movie Title',
          fillColor: Colors.white,
        ),
        // onChanged: (query) {},
        onSubmitted: bloc.addQuery,
      ),
    );
  }
}
